//Current window 
var win1 = Titanium.UI.createWindow({  
	    backgroundColor:'#000000',
	    navBarHidden:true,
	    exitOnClose: true
	});

var bullyBtn = Titanium.UI.createButton({
	    width      : 200,      //define the width of button
  	    height      : 50,      //define height of the button
        title         : 'Report-A-Bully'   //Define the text on button
	});
	    
	bullyBtn.addEventListener('click', function(){
		//creating an add session data window
			var Window=Titanium.UI.createWindow({
			url:'bully.js',
			title:'Report-A-Bully',
			backgroundColor:'#CCC',
			navBarHidden:true
		});
		Window.open();
	});
	win1.add(bullyBtn);

var newsBtn = Titanium.UI.createButton({
	    width      : 200,      //define the width of button
  	    height      : 50,      //define height of the button
        title         : 'News',   //Define the text on button
        top: 1
	});
	    
	newsBtn.addEventListener('click', function(){
		//creating an add session data window
			var Window=Titanium.UI.createWindow({
			url:'news.js',
			//title:'News',
			//backgroundColor:'#CCC',
			//navBarHidden:true
		});
		Window.open();
	});
	win1.add(newsBtn);
	
win1.open();