var bully = Ti.UI.currentWindow;

var windowWidth = Titanium.Platform.displayCaps.platformWidth;
var windowHeight = Titanium.Platform.displayCaps.platformHeight;
var osname = Ti.Platform.osname;
var tableData = [];


//**********************************************************************|
///////////////////////////////////////////////////////////////////////||
//////////////////////////////*SECTION 1*//////////////////////////////||
///////////////////////////////////////////////////////////////////////||
//**********************************************************************|

var row0 = Ti.UI.createTableViewRow({
                className:'aboutSection',
                //selectedBackgroundColor:'#00000000',
                rowIndex:0,
        });
        
var bullyLabel = Titanium.UI.createLabel({
	backgroundColor: 'black',
    text: 'Report-A-Bully',
    font:{fontSize:'21sp', fontWeight: 'bold'},
    width: windowWidth,
    height: windowHeight / 12,
    textAlign: 'center',
    top: 0,
    color: '#fff'
});

var infoLabel = Titanium.UI.createLabel({
	color: '#000000',
	text: 'Your Information (optional)',
	font:{fontFamily:'Arial', fontSize:'18sp', fontWeight:'bold'},
	textAlign:'center',
	width:'auto',
	top: windowHeight/10
});

var firstNameField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  hintText: 'First Name',
  top: windowHeight/7, 
  left: windowWidth/8,
  width: 400, 
  height: 60,
});

var lastNameField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  hintText: 'Last Name',
  top: windowHeight/4.7, 
  left: windowWidth/8,
  width: 400, 
  height: 60,
});

var rolePicker = Ti.UI.createPicker({
  top: windowHeight/3.5, 
  width: 400,
  height: 60
});

var roleData = [];
roleData[0]=Ti.UI.createPickerRow({title:'Role:'});
roleData[1]=Ti.UI.createPickerRow({title:'Bully'});
roleData[2]=Ti.UI.createPickerRow({title:'Victim'});
roleData[3]=Ti.UI.createPickerRow({title:'Witness'});
roleData[4]=Ti.UI.createPickerRow({title:'School Official'});
roleData[5]=Ti.UI.createPickerRow({title:'Family'});
roleData[6]=Ti.UI.createPickerRow({title:'Friend'});

rolePicker.add(roleData);
rolePicker.selectionIndicator = true;

var emailField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  hintText: 'Email Address',
  top: windowHeight/2.8, 
  left: windowWidth/8,
  width: 400, 
  height: 60,
});

var phoneField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  hintText: 'Phone #',
  top: windowHeight/2.35, 
  left: windowWidth/8,
  width: 400, 
  height: 60,
});

var campusPicker = Ti.UI.createPicker({
  top: windowHeight/2, 
  width: 400,
  height: 60
});

var campusData = [];
campusData[0]=Ti.UI.createPickerRow({title:'Campus:'});
campusData[1]=Ti.UI.createPickerRow({title:'Off Campus'});
campusData[2]=Ti.UI.createPickerRow({title:'ALLEN ES'});
campusData[3]=Ti.UI.createPickerRow({title:'BAKER MS'});
campusData[4]=Ti.UI.createPickerRow({title:'BARNES ES'});
campusData[5]=Ti.UI.createPickerRow({title:'BERLANGA ES'});
campusData[6]=Ti.UI.createPickerRow({title:'BROWNE MS'});
campusData[7]=Ti.UI.createPickerRow({title:'CALK ES'});
campusData[8]=Ti.UI.createPickerRow({title:'CARROLL HS'});
campusData[9]=Ti.UI.createPickerRow({title:'CLUB ESTATES ES'});
campusData[10]=Ti.UI.createPickerRow({title:'COLES'});
campusData[11]=Ti.UI.createPickerRow({title:'COLLEGIATE HS'});
campusData[12]=Ti.UI.createPickerRow({title:'CROCKETT ES'});
campusData[13]=Ti.UI.createPickerRow({title:'CULLEN MS'});
campusData[14]=Ti.UI.createPickerRow({title:'CUNNINGHAM MS'});
campusData[15]=Ti.UI.createPickerRow({title:'DAWSON ES'});
campusData[16]=Ti.UI.createPickerRow({title:'DRISCOLL MS'});
campusData[17]=Ti.UI.createPickerRow({title:'ECDC'});
campusData[18]=Ti.UI.createPickerRow({title:'EVANS ES'});
campusData[19]=Ti.UI.createPickerRow({title:'FANNIN ES'});
campusData[20]=Ti.UI.createPickerRow({title:'GALVAN ES'});
campusData[21]=Ti.UI.createPickerRow({title:'GARCIA ES'});
campusData[22]=Ti.UI.createPickerRow({title:'GIBSON ES'});
campusData[23]=Ti.UI.createPickerRow({title:'GLORIA HICKS ES'});
campusData[24]=Ti.UI.createPickerRow({title:'GRANT MS'});
campusData[25]=Ti.UI.createPickerRow({title:'HAAS MS'});
campusData[26]=Ti.UI.createPickerRow({title:'HAMLIN MS'});
campusData[27]=Ti.UI.createPickerRow({title:'HOUSTON ES'});
campusData[28]=Ti.UI.createPickerRow({title:'JONES ES'});
campusData[29]=Ti.UI.createPickerRow({title:'KAFFIE MS'});
campusData[30]=Ti.UI.createPickerRow({title:'KING HS'});
campusData[31]=Ti.UI.createPickerRow({title:'KOLDA ES'});
campusData[32]=Ti.UI.createPickerRow({title:'KOSTORYZ ES'});
campusData[33]=Ti.UI.createPickerRow({title:'LAMAR ES'});
campusData[34]=Ti.UI.createPickerRow({title:'LEXINGTON ES'});
campusData[35]=Ti.UI.createPickerRow({title:'LOS ENCINOS ES'});
campusData[36]=Ti.UI.createPickerRow({title:'MARTIN MS'});
campusData[37]=Ti.UI.createPickerRow({title:'MARY GRETT'});
campusData[38]=Ti.UI.createPickerRow({title:'MEADOWBROOK ES'});
campusData[39]=Ti.UI.createPickerRow({title:'MENGER ES'});
campusData[40]=Ti.UI.createPickerRow({title:'METRO E (WYNN SEALE)'});
campusData[41]=Ti.UI.createPickerRow({title:'MILLER METRO'});
campusData[42]=Ti.UI.createPickerRow({title:'MIRELES ES'});
campusData[43]=Ti.UI.createPickerRow({title:'MONTCLAIR ES'});
campusData[44]=Ti.UI.createPickerRow({title:'MOODY HS'});
campusData[45]=Ti.UI.createPickerRow({title:'MOORE ES'});
campusData[46]=Ti.UI.createPickerRow({title:'OAK PARK SES'});
campusData[47]=Ti.UI.createPickerRow({title:'PRESCOTT ES'});
campusData[48]=Ti.UI.createPickerRow({title:'RAY HS'});
campusData[49]=Ti.UI.createPickerRow({title:'SANDERS ES'});
campusData[50]=Ti.UI.createPickerRow({title:'SCHANEN ESTATES ES'});
campusData[51]=Ti.UI.createPickerRow({title:'SHAW SES'});
campusData[52]=Ti.UI.createPickerRow({title:'SLGC'});
campusData[53]=Ti.UI.createPickerRow({title:'SMITH ES'});
campusData[54]=Ti.UI.createPickerRow({title:'SOUTH PARK MS'});
campusData[55]=Ti.UI.createPickerRow({title:'TRAVIS ES'});
campusData[56]=Ti.UI.createPickerRow({title:'WEBB ES'});
campusData[57]=Ti.UI.createPickerRow({title:'WILSON ES'});
campusData[58]=Ti.UI.createPickerRow({title:'WINDSOR PARK ES'});
campusData[59]=Ti.UI.createPickerRow({title:'WOODLAWN ES'});
campusData[60]=Ti.UI.createPickerRow({title:'YEAGER ES'});
campusData[61]=Ti.UI.createPickerRow({title:'ZAVALA ES'});

campusPicker.add(campusData);
campusPicker.selectionIndicator = true;

var gradePicker = Ti.UI.createPicker({
  top: windowHeight/1.73, 
  width: 400,
  height: 60
});

var gradeData = [];
gradeData[0]=Ti.UI.createPickerRow({title:'Grade:'});
gradeData[1]=Ti.UI.createPickerRow({title:'Not Applicable'});
gradeData[2]=Ti.UI.createPickerRow({title:'PK'});
gradeData[3]=Ti.UI.createPickerRow({title:'KG'});
gradeData[4]=Ti.UI.createPickerRow({title:'01'});
gradeData[5]=Ti.UI.createPickerRow({title:'02'});
gradeData[6]=Ti.UI.createPickerRow({title:'03'});
gradeData[7]=Ti.UI.createPickerRow({title:'04'});
gradeData[8]=Ti.UI.createPickerRow({title:'05'});
gradeData[9]=Ti.UI.createPickerRow({title:'06'});
gradeData[10]=Ti.UI.createPickerRow({title:'07'});
gradeData[11]=Ti.UI.createPickerRow({title:'08'});
gradeData[12]=Ti.UI.createPickerRow({title:'09'});
gradeData[13]=Ti.UI.createPickerRow({title:'10'});
gradeData[14]=Ti.UI.createPickerRow({title:'11'});
gradeData[15]=Ti.UI.createPickerRow({title:'12'});

gradePicker.add(gradeData);
gradePicker.selectionIndicator = true;

row0.add(bullyLabel);
row0.add(infoLabel);
row0.add(firstNameField);
row0.add(lastNameField);
row0.add(rolePicker);
row0.add(emailField);
row0.add(phoneField);
row0.add(campusPicker);
row0.add(gradePicker);
tableData.push(row0);

//**********************************************************************|
///////////////////////////////////////////////////////////////////////||
//////////////////////////////*SECTION 2*//////////////////////////////||
///////////////////////////////////////////////////////////////////////||
//**********************************************************************|
var row1 = Ti.UI.createTableViewRow({
                className:'aboutSection',
               // selectedBackgroundColor:'#00000000',
                rowIndex:1,
        });
        
var whatHappenedLabel = Titanium.UI.createLabel({
	color: '#000000',
	text: 'What Happened?',
	font:{fontFamily:'Arial', fontSize:'18sp', fontWeight:'bold'},
	textAlign:'center',
	width:'auto',
	top: windowHeight/45
});

var dateField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  hintText: 'Date: When did this occur?',
  top: windowHeight/16, 
  left: windowWidth/8,
  width: 400, 
  height: 60,
});

var timePicker = Ti.UI.createPicker({
  top: windowHeight/7.6, 
  width: 400,
  height: 60
});

var timeData = [];
timeData[0]=Ti.UI.createPickerRow({title:'--Time--'});
timeData[1]=Ti.UI.createPickerRow({title:'12:00 AM'});
timeData[2]=Ti.UI.createPickerRow({title:'12:15 AM'});
timeData[3]=Ti.UI.createPickerRow({title:'12:30 AM'});
timeData[4]=Ti.UI.createPickerRow({title:'12:45 AM'});
timeData[5]=Ti.UI.createPickerRow({title:'1:00 AM'});
timeData[6]=Ti.UI.createPickerRow({title:'1:15 AM'});
timeData[7]=Ti.UI.createPickerRow({title:'1:30 AM'});
timeData[8]=Ti.UI.createPickerRow({title:'1:45 AM'});
timeData[9]=Ti.UI.createPickerRow({title:'2:00 AM'});
timeData[10]=Ti.UI.createPickerRow({title:'2:15 AM'});
timeData[11]=Ti.UI.createPickerRow({title:'2:30 AM'});
timeData[12]=Ti.UI.createPickerRow({title:'2:45 AM'});
timeData[13]=Ti.UI.createPickerRow({title:'3:00 AM'});
timeData[14]=Ti.UI.createPickerRow({title:'3:15 AM'});
timeData[15]=Ti.UI.createPickerRow({title:'3:30 AM'});
timeData[16]=Ti.UI.createPickerRow({title:'3:45 AM'});
timeData[17]=Ti.UI.createPickerRow({title:'4:00 AM'});
timeData[18]=Ti.UI.createPickerRow({title:'4:15 AM'});
timeData[19]=Ti.UI.createPickerRow({title:'4:30 AM'});
timeData[20]=Ti.UI.createPickerRow({title:'4:45 AM'});
timeData[21]=Ti.UI.createPickerRow({title:'5:00 AM'});
timeData[22]=Ti.UI.createPickerRow({title:'5:15 AM'});
timeData[23]=Ti.UI.createPickerRow({title:'5:30 AM'});
timeData[24]=Ti.UI.createPickerRow({title:'5:45 AM'});
timeData[25]=Ti.UI.createPickerRow({title:'6:00 AM'});
timeData[26]=Ti.UI.createPickerRow({title:'6:15 AM'});
timeData[27]=Ti.UI.createPickerRow({title:'6:30 AM'});
timeData[28]=Ti.UI.createPickerRow({title:'6:45 AM'});
timeData[29]=Ti.UI.createPickerRow({title:'7:00 AM'});
timeData[30]=Ti.UI.createPickerRow({title:'7:15 AM'});
timeData[31]=Ti.UI.createPickerRow({title:'7:30 AM'});
timeData[32]=Ti.UI.createPickerRow({title:'7:45 AM'});
timeData[33]=Ti.UI.createPickerRow({title:'8:00 AM'});
timeData[34]=Ti.UI.createPickerRow({title:'8:15 AM'});
timeData[35]=Ti.UI.createPickerRow({title:'8:30 AM'});
timeData[36]=Ti.UI.createPickerRow({title:'8:45 AM'});
timeData[37]=Ti.UI.createPickerRow({title:'9:00 AM'});
timeData[38]=Ti.UI.createPickerRow({title:'9:15 AM'});
timeData[39]=Ti.UI.createPickerRow({title:'9:30 AM'});
timeData[40]=Ti.UI.createPickerRow({title:'9:45 AM'});
timeData[41]=Ti.UI.createPickerRow({title:'10:00 AM'});
timeData[42]=Ti.UI.createPickerRow({title:'10:15 AM'});
timeData[43]=Ti.UI.createPickerRow({title:'10:30 AM'});
timeData[44]=Ti.UI.createPickerRow({title:'10:45 AM'});
timeData[45]=Ti.UI.createPickerRow({title:'11:00 AM'});
timeData[46]=Ti.UI.createPickerRow({title:'11:15 AM'});
timeData[47]=Ti.UI.createPickerRow({title:'11:30 AM'});
timeData[48]=Ti.UI.createPickerRow({title:'11:45 AM'});
timeData[49]=Ti.UI.createPickerRow({title:'12:00 PM'});
timeData[50]=Ti.UI.createPickerRow({title:'12:15 PM'});
timeData[51]=Ti.UI.createPickerRow({title:'12:30 PM'});
timeData[52]=Ti.UI.createPickerRow({title:'12:45 PM'});
timeData[53]=Ti.UI.createPickerRow({title:'1:00 PM'});
timeData[54]=Ti.UI.createPickerRow({title:'1:15 PM'});
timeData[55]=Ti.UI.createPickerRow({title:'1:30 PM'});
timeData[56]=Ti.UI.createPickerRow({title:'1:45 PM'});
timeData[57]=Ti.UI.createPickerRow({title:'2:00 PM'});
timeData[58]=Ti.UI.createPickerRow({title:'2:15 PM'});
timeData[59]=Ti.UI.createPickerRow({title:'2:30 PM'});
timeData[60]=Ti.UI.createPickerRow({title:'2:45 PM'});
timeData[61]=Ti.UI.createPickerRow({title:'3:00 PM'});
timeData[62]=Ti.UI.createPickerRow({title:'3:15 PM'});
timeData[63]=Ti.UI.createPickerRow({title:'3:30 PM'});
timeData[64]=Ti.UI.createPickerRow({title:'3:45 PM'});
timeData[65]=Ti.UI.createPickerRow({title:'4:00 PM'});
timeData[66]=Ti.UI.createPickerRow({title:'4:15 PM'});
timeData[67]=Ti.UI.createPickerRow({title:'4:30 PM'});
timeData[68]=Ti.UI.createPickerRow({title:'4:45 PM'});
timeData[69]=Ti.UI.createPickerRow({title:'5:00 PM'});
timeData[70]=Ti.UI.createPickerRow({title:'5:15 PM'});
timeData[71]=Ti.UI.createPickerRow({title:'5:30 PM'});
timeData[72]=Ti.UI.createPickerRow({title:'5:45 PM'});
timeData[73]=Ti.UI.createPickerRow({title:'6:00 PM'});
timeData[74]=Ti.UI.createPickerRow({title:'6:15 PM'});
timeData[75]=Ti.UI.createPickerRow({title:'6:30 PM'});
timeData[76]=Ti.UI.createPickerRow({title:'6:45 PM'});
timeData[77]=Ti.UI.createPickerRow({title:'7:00 PM'});
timeData[78]=Ti.UI.createPickerRow({title:'7:15 PM'});
timeData[79]=Ti.UI.createPickerRow({title:'7:30 PM'});
timeData[80]=Ti.UI.createPickerRow({title:'7:45 PM'});
timeData[81]=Ti.UI.createPickerRow({title:'8:00 PM'});
timeData[82]=Ti.UI.createPickerRow({title:'8:15 PM'});
timeData[83]=Ti.UI.createPickerRow({title:'8:30 PM'});
timeData[84]=Ti.UI.createPickerRow({title:'8:45 PM'});
timeData[85]=Ti.UI.createPickerRow({title:'9:00 PM'});
timeData[86]=Ti.UI.createPickerRow({title:'9:15 PM'});
timeData[87]=Ti.UI.createPickerRow({title:'9:30 PM'});
timeData[88]=Ti.UI.createPickerRow({title:'9:45 PM'});
timeData[89]=Ti.UI.createPickerRow({title:'10:00 PM'});
timeData[90]=Ti.UI.createPickerRow({title:'10:15 PM'});
timeData[91]=Ti.UI.createPickerRow({title:'10:30 PM'});
timeData[92]=Ti.UI.createPickerRow({title:'10:45 PM'});
timeData[93]=Ti.UI.createPickerRow({title:'11:00 PM'});
timeData[94]=Ti.UI.createPickerRow({title:'11:15 PM'});
timeData[95]=Ti.UI.createPickerRow({title:'11:30 PM'});
timeData[96]=Ti.UI.createPickerRow({title:'11:45 PM'});

timePicker.add(timeData);
timePicker.selectionIndicator = true;

var wherePicker = Ti.UI.createPicker({
  top: windowHeight/4.9, 
  width: 400,
  height: 60
});

var whereData = [];
whereData[0]=Ti.UI.createPickerRow({title:'Where did it occur?:'});
whereData[1]=Ti.UI.createPickerRow({title:'Off Campus'});
whereData[2]=Ti.UI.createPickerRow({title:'ALLEN ES'});
whereData[3]=Ti.UI.createPickerRow({title:'BAKER MS'});
whereData[4]=Ti.UI.createPickerRow({title:'BARNES ES'});
whereData[5]=Ti.UI.createPickerRow({title:'BERLANGA ES'});
whereData[6]=Ti.UI.createPickerRow({title:'BROWNE MS'});
whereData[7]=Ti.UI.createPickerRow({title:'CALK ES'});
whereData[8]=Ti.UI.createPickerRow({title:'CARROLL HS'});
whereData[9]=Ti.UI.createPickerRow({title:'CLUB ESTATES ES'});
whereData[10]=Ti.UI.createPickerRow({title:'COLES'});
whereData[11]=Ti.UI.createPickerRow({title:'COLLEGIATE HS'});
whereData[12]=Ti.UI.createPickerRow({title:'CROCKETT ES'});
whereData[13]=Ti.UI.createPickerRow({title:'CULLEN MS'});
whereData[14]=Ti.UI.createPickerRow({title:'CUNNINGHAM MS'});
whereData[15]=Ti.UI.createPickerRow({title:'DAWSON ES'});
whereData[16]=Ti.UI.createPickerRow({title:'DRISCOLL MS'});
whereData[17]=Ti.UI.createPickerRow({title:'ECDC'});
whereData[18]=Ti.UI.createPickerRow({title:'EVANS ES'});
whereData[19]=Ti.UI.createPickerRow({title:'FANNIN ES'});
whereData[20]=Ti.UI.createPickerRow({title:'GALVAN ES'});
whereData[21]=Ti.UI.createPickerRow({title:'GARCIA ES'});
whereData[22]=Ti.UI.createPickerRow({title:'GIBSON ES'});
whereData[23]=Ti.UI.createPickerRow({title:'GLORIA HICKS ES'});
whereData[24]=Ti.UI.createPickerRow({title:'GRANT MS'});
whereData[25]=Ti.UI.createPickerRow({title:'HAAS MS'});
whereData[26]=Ti.UI.createPickerRow({title:'HAMLIN MS'});
whereData[27]=Ti.UI.createPickerRow({title:'HOUSTON ES'});
whereData[28]=Ti.UI.createPickerRow({title:'JONES ES'});
whereData[29]=Ti.UI.createPickerRow({title:'KAFFIE MS'});
whereData[30]=Ti.UI.createPickerRow({title:'KING HS'});
whereData[31]=Ti.UI.createPickerRow({title:'KOLDA ES'});
whereData[32]=Ti.UI.createPickerRow({title:'KOSTORYZ ES'});
whereData[33]=Ti.UI.createPickerRow({title:'LAMAR ES'});
whereData[34]=Ti.UI.createPickerRow({title:'LEXINGTON ES'});
whereData[35]=Ti.UI.createPickerRow({title:'LOS ENCINOS ES'});
whereData[36]=Ti.UI.createPickerRow({title:'MARTIN MS'});
whereData[37]=Ti.UI.createPickerRow({title:'MARY GRETT'});
whereData[38]=Ti.UI.createPickerRow({title:'MEADOWBROOK ES'});
whereData[39]=Ti.UI.createPickerRow({title:'MENGER ES'});
whereData[40]=Ti.UI.createPickerRow({title:'METRO E (WYNN SEALE)'});
whereData[41]=Ti.UI.createPickerRow({title:'MILLER METRO'});
whereData[42]=Ti.UI.createPickerRow({title:'MIRELES ES'});
whereData[43]=Ti.UI.createPickerRow({title:'MONTCLAIR ES'});
whereData[44]=Ti.UI.createPickerRow({title:'MOODY HS'});
whereData[45]=Ti.UI.createPickerRow({title:'MOORE ES'});
whereData[46]=Ti.UI.createPickerRow({title:'OAK PARK SES'});
whereData[47]=Ti.UI.createPickerRow({title:'PRESCOTT ES'});
whereData[48]=Ti.UI.createPickerRow({title:'RAY HS'});
whereData[49]=Ti.UI.createPickerRow({title:'SANDERS ES'});
whereData[50]=Ti.UI.createPickerRow({title:'SCHANEN ESTATES ES'});
whereData[51]=Ti.UI.createPickerRow({title:'SHAW SES'});
whereData[52]=Ti.UI.createPickerRow({title:'SLGC'});
whereData[53]=Ti.UI.createPickerRow({title:'SMITH ES'});
whereData[54]=Ti.UI.createPickerRow({title:'SOUTH PARK MS'});
whereData[55]=Ti.UI.createPickerRow({title:'TRAVIS ES'});
whereData[56]=Ti.UI.createPickerRow({title:'WEBB ES'});
whereData[57]=Ti.UI.createPickerRow({title:'WILSON ES'});
whereData[58]=Ti.UI.createPickerRow({title:'WINDSOR PARK ES'});
whereData[59]=Ti.UI.createPickerRow({title:'WOODLAWN ES'});
whereData[60]=Ti.UI.createPickerRow({title:'YEAGER ES'});
whereData[61]=Ti.UI.createPickerRow({title:'ZAVALA ES'});

wherePicker.add(whereData);
wherePicker.selectionIndicator = true;

var descriptionTextArea = Ti.UI.createTextArea({
  textAlign: 'left',
  hintText: 'Please describe the incident and location: (Cafeteria, Hallway, Restroom, Playground, etc)',
  top: windowHeight/3.7,
  width: 400, height: 300
});

//adding items to row 1
row1.add(whatHappenedLabel);
row1.add(dateField);
row1.add(timePicker);
row1.add(wherePicker);
row1.add(descriptionTextArea);
tableData.push(row1);

//**********************************************************************|
///////////////////////////////////////////////////////////////////////||
//////////////////////////////*SECTION 3*//////////////////////////////||
///////////////////////////////////////////////////////////////////////||
//**********************************************************************|
var row2 = Ti.UI.createTableViewRow({
                className:'aboutSection',
//                selectedBackgroundColor:'#00000000',
                rowIndex:2,
        });
        
var whoWhatLabel = Titanium.UI.createLabel({
	color: '#000000',
	text: 'Information about who was there, what role they played, and/or who you talked to about the incident',
	font:{fontFamily:'Arial', fontSize:'18sp', fontWeight:'bold'},
	textAlign:'center',
	width:'auto',
	top: windowHeight/45
});
       
var whofirstNameField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  hintText: 'First Name',
  top: windowHeight/7, 
  left: windowWidth/8,
  width: 400, 
  height: 60,
});

var wholastNameField = Ti.UI.createTextField({
  borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  hintText: 'Last Name',
  top: windowHeight/4.8, 
  left: windowWidth/8,
  width: 400, 
  height: 60,
});

var whorolePicker = Ti.UI.createPicker({
  top: windowHeight/3.6, 
  width: 400,
  height: 60
});

var whoroleData = [];
whoroleData[0]=Ti.UI.createPickerRow({title:'Role:'});
whoroleData[1]=Ti.UI.createPickerRow({title:'Bully'});
whoroleData[2]=Ti.UI.createPickerRow({title:'Victim'});
whoroleData[3]=Ti.UI.createPickerRow({title:'Witness'});
whoroleData[4]=Ti.UI.createPickerRow({title:'School Official'});
whoroleData[5]=Ti.UI.createPickerRow({title:'Family'});
whoroleData[6]=Ti.UI.createPickerRow({title:'Friend'});

whorolePicker.add(whoroleData);
whorolePicker.selectionIndicator = true;

var genderPicker = Ti.UI.createPicker({
  top: windowHeight/2.9, 
  width: 400,
  height: 60
});

var genderData = [];
genderData[0]=Ti.UI.createPickerRow({title:'Gender:'});
genderData[1]=Ti.UI.createPickerRow({title:'Male'});
genderData[2]=Ti.UI.createPickerRow({title:'Female'});

genderPicker.add(genderData);
genderPicker.selectionIndicator = true;

var whogradePicker = Ti.UI.createPicker({
  top: windowHeight/2.4, 
  width: 400,
  height: 60
});

var whogradeData = [];
whogradeData[0]=Ti.UI.createPickerRow({title:'Grade:'});
whogradeData[1]=Ti.UI.createPickerRow({title:'Not Applicable'});
whogradeData[2]=Ti.UI.createPickerRow({title:'PK'});
whogradeData[3]=Ti.UI.createPickerRow({title:'KG'});
whogradeData[4]=Ti.UI.createPickerRow({title:'01'});
whogradeData[5]=Ti.UI.createPickerRow({title:'02'});
whogradeData[6]=Ti.UI.createPickerRow({title:'03'});
whogradeData[7]=Ti.UI.createPickerRow({title:'04'});
whogradeData[8]=Ti.UI.createPickerRow({title:'05'});
whogradeData[9]=Ti.UI.createPickerRow({title:'06'});
whogradeData[10]=Ti.UI.createPickerRow({title:'07'});
whogradeData[11]=Ti.UI.createPickerRow({title:'08'});
whogradeData[12]=Ti.UI.createPickerRow({title:'09'});
whogradeData[13]=Ti.UI.createPickerRow({title:'10'});
whogradeData[14]=Ti.UI.createPickerRow({title:'11'});
whogradeData[15]=Ti.UI.createPickerRow({title:'12'});

whogradePicker.add(whogradeData);
whogradePicker.selectionIndicator = true;

var knowPersonPicker = Ti.UI.createPicker({
  top: windowHeight/2.057, 
  width: 400,
  height: 60
});

var knowPersonData = [];
knowPersonData[0]=Ti.UI.createPickerRow({title:'Do you know this person?:'});
knowPersonData[1]=Ti.UI.createPickerRow({title:'Yes'});
knowPersonData[2]=Ti.UI.createPickerRow({title:'No'});

knowPersonPicker.add(knowPersonData);
knowPersonPicker.selectionIndicator = true;

var racePicker = Ti.UI.createPicker({
  top: windowHeight/1.8, 
  width: 400,
  height: 60
});

var raceData = [];
raceData[0]=Ti.UI.createPickerRow({title:'Race:'});
raceData[1]=Ti.UI.createPickerRow({title:'American Indian or Alaskan Native'});
raceData[2]=Ti.UI.createPickerRow({title:'Asian'});
raceData[3]=Ti.UI.createPickerRow({title:'African American'});
raceData[4]=Ti.UI.createPickerRow({title:'Hispanic'});
raceData[5]=Ti.UI.createPickerRow({title:'Pacific Islander'});
raceData[6]=Ti.UI.createPickerRow({title:'White'});
raceData[7]=Ti.UI.createPickerRow({title:'Other'});

racePicker.add(raceData);
racePicker.selectionIndicator = true;

var whocampusPicker = Ti.UI.createPicker({
  top: windowHeight/1.8, 
  width: 400,
  height: 60
});

var whocampusData = [];
whocampusData[0]=Ti.UI.createPickerRow({title:'Campus:'});
whocampusData[1]=Ti.UI.createPickerRow({title:'Off Campus'});
whocampusData[2]=Ti.UI.createPickerRow({title:'ALLEN ES'});
whocampusData[3]=Ti.UI.createPickerRow({title:'BAKER MS'});
whocampusData[4]=Ti.UI.createPickerRow({title:'BARNES ES'});
whocampusData[5]=Ti.UI.createPickerRow({title:'BERLANGA ES'});
whocampusData[6]=Ti.UI.createPickerRow({title:'BROWNE MS'});
whocampusData[7]=Ti.UI.createPickerRow({title:'CALK ES'});
whocampusData[8]=Ti.UI.createPickerRow({title:'CARROLL HS'});
whocampusData[9]=Ti.UI.createPickerRow({title:'CLUB ESTATES ES'});
whocampusData[10]=Ti.UI.createPickerRow({title:'COLES'});
whocampusData[11]=Ti.UI.createPickerRow({title:'COLLEGIATE HS'});
whocampusData[12]=Ti.UI.createPickerRow({title:'CROCKETT ES'});
whocampusData[13]=Ti.UI.createPickerRow({title:'CULLEN MS'});
whocampusData[14]=Ti.UI.createPickerRow({title:'CUNNINGHAM MS'});
whocampusData[15]=Ti.UI.createPickerRow({title:'DAWSON ES'});
whocampusData[16]=Ti.UI.createPickerRow({title:'DRISCOLL MS'});
whocampusData[17]=Ti.UI.createPickerRow({title:'ECDC'});
whocampusData[18]=Ti.UI.createPickerRow({title:'EVANS ES'});
whocampusData[19]=Ti.UI.createPickerRow({title:'FANNIN ES'});
whocampusData[20]=Ti.UI.createPickerRow({title:'GALVAN ES'});
whocampusData[21]=Ti.UI.createPickerRow({title:'GARCIA ES'});
whocampusData[22]=Ti.UI.createPickerRow({title:'GIBSON ES'});
whocampusData[23]=Ti.UI.createPickerRow({title:'GLORIA HICKS ES'});
whocampusData[24]=Ti.UI.createPickerRow({title:'GRANT MS'});
whocampusData[25]=Ti.UI.createPickerRow({title:'HAAS MS'});
whocampusData[26]=Ti.UI.createPickerRow({title:'HAMLIN MS'});
whocampusData[27]=Ti.UI.createPickerRow({title:'HOUSTON ES'});
whocampusData[28]=Ti.UI.createPickerRow({title:'JONES ES'});
whocampusData[29]=Ti.UI.createPickerRow({title:'KAFFIE MS'});
whocampusData[30]=Ti.UI.createPickerRow({title:'KING HS'});
whocampusData[31]=Ti.UI.createPickerRow({title:'KOLDA ES'});
whocampusData[32]=Ti.UI.createPickerRow({title:'KOSTORYZ ES'});
whocampusData[33]=Ti.UI.createPickerRow({title:'LAMAR ES'});
whocampusData[34]=Ti.UI.createPickerRow({title:'LEXINGTON ES'});
whocampusData[35]=Ti.UI.createPickerRow({title:'LOS ENCINOS ES'});
whocampusData[36]=Ti.UI.createPickerRow({title:'MARTIN MS'});
whocampusData[37]=Ti.UI.createPickerRow({title:'MARY GRETT'});
whocampusData[38]=Ti.UI.createPickerRow({title:'MEADOWBROOK ES'});
whocampusData[39]=Ti.UI.createPickerRow({title:'MENGER ES'});
whocampusData[40]=Ti.UI.createPickerRow({title:'METRO E (WYNN SEALE)'});
whocampusData[41]=Ti.UI.createPickerRow({title:'MILLER METRO'});
whocampusData[42]=Ti.UI.createPickerRow({title:'MIRELES ES'});
whocampusData[43]=Ti.UI.createPickerRow({title:'MONTCLAIR ES'});
whocampusData[44]=Ti.UI.createPickerRow({title:'MOODY HS'});
whocampusData[45]=Ti.UI.createPickerRow({title:'MOORE ES'});
whocampusData[46]=Ti.UI.createPickerRow({title:'OAK PARK SES'});
whocampusData[47]=Ti.UI.createPickerRow({title:'PRESCOTT ES'});
whocampusData[48]=Ti.UI.createPickerRow({title:'RAY HS'});
whocampusData[49]=Ti.UI.createPickerRow({title:'SANDERS ES'});
whocampusData[50]=Ti.UI.createPickerRow({title:'SCHANEN ESTATES ES'});
whocampusData[51]=Ti.UI.createPickerRow({title:'SHAW SES'});
whocampusData[52]=Ti.UI.createPickerRow({title:'SLGC'});
whocampusData[53]=Ti.UI.createPickerRow({title:'SMITH ES'});
whocampusData[54]=Ti.UI.createPickerRow({title:'SOUTH PARK MS'});
whocampusData[55]=Ti.UI.createPickerRow({title:'TRAVIS ES'});
whocampusData[56]=Ti.UI.createPickerRow({title:'WEBB ES'});
whocampusData[57]=Ti.UI.createPickerRow({title:'WILSON ES'});
whocampusData[58]=Ti.UI.createPickerRow({title:'WINDSOR PARK ES'});
whocampusData[59]=Ti.UI.createPickerRow({title:'WOODLAWN ES'});
whocampusData[60]=Ti.UI.createPickerRow({title:'YEAGER ES'});
whocampusData[61]=Ti.UI.createPickerRow({title:'ZAVALA ES'});

whocampusPicker.add(whocampusData);
whocampusPicker.selectionIndicator = true;

var personDescriptionTextArea = Ti.UI.createTextArea({
  textAlign: 'left',
  hintText: 'Description about the person',
  top: windowHeight/1.6,
  width: 400, height: 200
});

var addPersonbtn = Titanium.UI.createButton({
	    width: 400,      //define the width of button
  	    height: 60,      //define height of the button
  	    backgroundColor: '#D0463F',
  	    font: {fontSize: '18sp', fontWeight: 'bold'},
        title: 'Add Person',   //Define the text on button
        color: '#fff',
        borderRadius: 5,
        top: windowHeight/1.15
	});

/*	addPersonbtn.addEventListener('click', function(){
		//creating an add session data window
			var Window=Titanium.UI.createWindow({
			url:'bully.js',
			title:'Report-A-Bully',
			backgroundColor:'#CCC',
			navBarHidden:true
		});*/
		
var submitBtn = Titanium.UI.createButton({
	    width: 400,      //define the width of button
  	    height: 60,      //define height of the button
  	    backgroundColor: '#57B057',
  	    font: {fontSize: '18sp', fontWeight: 'bold'},
        title: 'Submit',   //Define the text on button
        color: '#fff',
        borderRadius: 5,
        top: windowHeight/1.05
	});
	
/*var submitBtn = Titanium.UI.createButton({
	    width: 400,      //define the width of button
  	    height: 60,      //define height of the button
  	    backgroundColor: 'green',
  	    font: {fontSize: '18sp', fontWeight: 'bold'},
        title: 'Submit',   //Define the text on button
        color: '#fff',
        borderRadius: 5,
        top: windowHeight/10
	});*/	
	   
row2.add(whoWhatLabel);
row2.add(whofirstNameField);
row2.add(wholastNameField);
row2.add(whorolePicker);
row2.add(genderPicker);
row2.add(whogradePicker)
row2.add(knowPersonPicker);
row2.add(racePicker);
row2.add(whocampusPicker);
row2.add(personDescriptionTextArea);
row2.add(addPersonbtn);
row2.add(submitBtn);
tableData.push(row2);

//**********************************************************************|
var tableView = Ti.UI.createTableView({
            backgroundImage:'/ui/common/images/navy_blue.png',
            data:tableData
        });

        bully.add(tableView);
        
//**********************************************************************|

//capturing back button action in order to close window 
bully.addEventListener('android:back',function(e) {
	bully.close(Ti.UI.currentWindow);
});