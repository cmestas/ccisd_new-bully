//Titanium.UI.setBackgroundColor('#000');
var windowWidth = Titanium.Platform.displayCaps.platformWidth;
var windowHeight = Titanium.Platform.displayCaps.platformHeight;

var win = Titanium.UI.createWindow({  
    title:'News',
    backgroundColor:'#CCC'
});

var data = [];

// create table view
var tableview = Titanium.UI.createTableView({
	data:data,
	//headerTitle: 'News',
	//backgroundColor:'#CCC',
});

win.add(tableview);

var row0 = Ti.UI.createTableViewRow({
                className:'aboutSection',
                //selectedBackgroundColor:'#00000000',
                rowIndex:0,
        });
        
var newsLabel = Titanium.UI.createLabel({
	backgroundColor: 'black',
    text: 'News',
    font:{fontSize:'21sp', fontWeight: 'bold'},
    width: windowWidth,
    height: windowHeight / 12,
    textAlign: 'center',
    top: 0,
    color: '#fff'
});
row0.add(newsLabel);
tableview.appendRow(row0);

var xhr = Titanium.Network.createHTTPClient();

xhr.onload = function()
{

	try
	{
		var doc = this.responseXML.documentElement;
		var items = doc.getElementsByTagName('item');
		var doctitle = doc.evaluate("//channel/title/text()").item(0).nodeValue;

		var urls = new Array();

		for(var c=0; c<items.length;c++)
		{
			urls[c] = items.item(c).getElementsByTagName('link').item(0).text;

			postName = items.item(c).getElementsByTagName('title').item(0).text;
			postUrl = items.item(c).getElementsByTagName('link').item(0).text;


			row = Titanium.UI.createTableViewRow({
				title: postName,
				backgroundColor:'#CCC',
				color: 'black',
				font: {fontSize: '16sp'}
			});

			if(c == 0)
			{
				row.header = 'Query7 RSS Feed';
			}

			row.addEventListener('click', function (e){

			Ti.API.info('>>>>>' + e.index);

			var intent = Titanium.Android.createIntent({

				action: Titanium.Android.ACTION_VIEW,
				data: urls[e.index],

			});

			intent.addCategory(Titanium.Android.CATEGORY_BROWSABLE);
			Ti.Android.currentActivity.startActivity(intent);


			});
			tableview.appendRow(row);

		}

	}
	catch(E)
	{
		alert(E);
	}

};

xhr.open('GET', 'http://feeds.feedburner.com/query7blog.rss');
xhr.send();

win.open();